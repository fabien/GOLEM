# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* golem_activity_place
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-01 22:35+0000\n"
"PO-Revision-Date: 2017-05-01 22:35+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. module: golem_activity_place
#: model:ir.model.fields,field_description:golem_activity_place.field_golem_activity_is_overbooked
msgid "Allow overbook?"
msgstr "Autoriser la surcharge ?"

#. module: golem_activity_place
#: model:ir.model,name:golem_activity_place.model_golem_activity
msgid "GOLEM Activity"
msgstr "Activité"

#. module: golem_activity_place
#: model:ir.model.fields,field_description:golem_activity_place.field_golem_activity_places_min
msgid "Minimum places"
msgstr "Places minimum"

#. module: golem_activity_place
#: model:ir.model.fields,help:golem_activity_place.field_golem_activity_places_min
msgid "Minimum places to maintain the activity"
msgstr "Nombre de places minimales pour maintenir l'activité"

#. module: golem_activity_place
#: code:addons/golem_activity_place/models/golem_activity.py:57
#, python-format
msgid "Number of places cannot be negative."
msgstr "Le nombre de places ne peut pas être négatif"

#. module: golem_activity_place
#: code:addons/golem_activity_place/models/golem_activity.py:61
#, python-format
msgid "Overbooked places cannot be inferior than places"
msgstr "Le nombre de places avec surcharge ne peut être inférieur aux places"

#. module: golem_activity_place
#: model:ir.model.fields,field_description:golem_activity_place.field_golem_activity_places_overbooked
msgid "Places with overbook"
msgstr "Places avec surcharge"
